﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
    public class Measurement
    {
        public int Id { get; set; }
        public float Temp { get; set; }
        public float Humidity { get; set; }
        public DateTime Time {get; set; }

        public Measurement()
        {
            Time = DateTime.Now;
        }
    }
}
