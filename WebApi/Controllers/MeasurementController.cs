﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Measurement")]
    public class MeasurementController : Controller
    {
        private readonly MeasurementContext _context;

        public MeasurementController(MeasurementContext context)
        {
            _context = context;
            _context.Measurements.Add(new Measurement {Humidity = 65.2F, Temp = 25.7F});
            _context.SaveChanges();
        }

        [HttpGet(Name = "Measurements")]
        public List<Measurement> GetAll()
        {
            return _context.Measurements.ToList();
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var item = _context.Measurements.Find(id);
            if (item == null) return NotFound();
            return Ok(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Measurement item)
        {
            if (item == null) return BadRequest();

            _context.Measurements.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("Measurements", new {id = item.Id}, item);
        }
    }
}